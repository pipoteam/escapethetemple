﻿using UnityEngine;
using System.Collections;
using Prime31;

public class PlayerMovement : MonoBehaviour {

	[Range(-5f,-45f)]
	public float Gravity = -25f;
	[Range(2f,15f)]
	public float RunSpeed = 8f;
	[Range(0f,30f)]
	public float GroundDamping = 20f; // how fast do we change direction? higher means faster
	private float tmp_GroundDamping; //this ground damping value is for when we dash

	[Range(0f,10f)]
	public float InAirDamping = 5f;
	[Range(1f,8f)]
	public float jumpHeight = 4f;

	private bool CanMove = true;
	//

	private int CurrentDirection=0; 
	private Vector3 Movement;

	//private vars
	private CharacterController2D _controller;
	private Animator _animator;
	private RaycastHit2D _lastControllerColliderHit;


	//Awake hecho por mi
	void Awake() {
		_animator = GetComponent<Animator>();
		_controller = GetComponent<CharacterController2D>();
		
		// listen to some events for illustration purposes
		_controller.onControllerCollidedEvent += onControllerCollider;
		_controller.onTriggerEnterEvent += onTriggerEnterEvent;
		_controller.onTriggerExitEvent += onTriggerExitEvent;


		//remember the ground damping value
		tmp_GroundDamping = GroundDamping;
	}


	// Use this for initialization
	void Start () {	}


	#region Event Listeners
	
	void onControllerCollider( RaycastHit2D hit )
	{
		// bail out on plain old ground hits cause they arent very interesting
		if( hit.normal.y == 1f )
			return;
		
		// logs any collider hits if uncommented. it gets noisy so it is commented out for the demo
		//Debug.Log( "flags: " + _controller.collisionState + ", hit.normal: " + hit.normal );
	}

	void onTriggerEnterEvent( Collider2D col )
	{
		//Debug.Log( "onTriggerExitEvent: " + col.gameObject.name );
	}
	
	void onTriggerExitEvent( Collider2D col )
	{
		//Debug.Log( "onTriggerExitEvent: " + col.gameObject.name );
	}
	
	#endregion



	// Update called AFTER the normal update
	void Update () {
		//
		if (_animator) {

			Movement = _controller.velocity;

			if ( _controller.isGrounded ) Movement.y = 0;

			//Dash overrides movement
			if (CanMove) {
				if (Input.GetAxis("Horizontal") > 0) {
					CurrentDirection = 1;
					if( transform.localScale.x < 0f ) transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z );
					if( _controller.isGrounded ) _animator.Play( Animator.StringToHash( "Run" ) );
				}else if( Input.GetAxis("Horizontal") <0 ) {
					CurrentDirection = -1;
					if( transform.localScale.x > 0f ) transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z );
					if( _controller.isGrounded ) _animator.Play( Animator.StringToHash( "Run" ) );
				}else {
					CurrentDirection = 0;
					if( _controller.isGrounded ) { //mini-section for Idle, Duck and Block
						if (Input.GetAxis("Vertical") < 0) {
							_animator.Play( Animator.StringToHash( "Duck" ) );
						}else {
							_animator.Play( Animator.StringToHash( "Idle" ) );
						}
					}
				}
			}


			//we can only fall whilst in air.
			if( !_controller.isGrounded && Movement.y < -5f) {
				_animator.Play( Animator.StringToHash("Fall") );
			}




			// we can only jump whilst grounded
			//if( _controller.isGrounded && Input.GetButtonDown ("BUTTON_A") ) {
			if( _controller.isGrounded && Input.GetButtonDown("BUTTON_A") ) {
				Movement.y = Mathf.Sqrt( 2f * jumpHeight * -Gravity );
				_animator.Play( Animator.StringToHash( "Jump" ) );
			}

			//we can attack or use secondaryweapons whilst jumping, grounded, and duck
			if( Input.GetButtonDown("BUTTON_X") ) {
				//_animator.Play( Animator.StringToHash("Attack_L") );
			}else if( Input.GetButtonDown("BUTTON_B") ) {
				_animator.Play( Animator.StringToHash("Attack_R") );
				CanMove = false;
				StartCoroutine("RecoverControl",0.2f);
			}else if( Input.GetButtonDown("BUTTON_Y") ) {
				//_animator.Play( Animator.StringToHash("Attack_B") );
//			}else if( Input.GetButtonDown("BUTTON_RB") ) {
//				//_animator.Play( Animator.StringToHash("Secondary_Use") );
			}


			/*
			//we can only Block whilst grounded
			if( _controller.isGrounded && Input.GetButton("BUTTON_LB") ) {
				_animator.Play( Animator.StringToHash("Block") );
			}
			*/


//			//we can change between secondarys in any moment
//			if (Input.GetAxis("PAD_Vertical") >0.5f ) {
//				//_animator.Play( Animator.StringToHash("Secondary_Next") );
//			}else if (Input.GetAxis("PAD_Vertical") < -0.5f ) {
//				//_animator.Play( Animator.StringToHash("Secondary_Prev") );
//			}


			//aplica el filtro de suavidad de direccion
			float smoothedMovementFactor = _controller.isGrounded ? GroundDamping : InAirDamping; // how fast do we change direction?
			Movement.x = Mathf.Lerp( Movement.x , CurrentDirection * RunSpeed , Time.deltaTime * smoothedMovementFactor );


			// apply gravity before moving
			Movement.y += Gravity * Time.deltaTime;

			//final step, we move the controller
			_controller.move( Movement * Time.deltaTime );



		}
	}


	//Async COroutine that disables the movement/control for 1sec.
	IEnumerator RecoverControl(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		CanMove = true;
		GroundDamping = tmp_GroundDamping;
	}

}
