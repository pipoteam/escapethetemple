﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class LavaController : MonoBehaviour {

	public float speed = 1f;
	public float initial_delaytoraise = 5f;
	public float delaytoraise = 5f;

	void Awake() {
		DOTween.Init();
	}

	void Start() {
		InvokeRepeating("RaiseLava",initial_delaytoraise,delaytoraise);
	}
	
	//sube la lava
	public void RaiseLava() {
		//transform.Translate(new Vector3(0,  speed * Time.deltaTime,0));
		transform.DOLocalMoveY(transform.position.y + speed, 5f);
	}
}
